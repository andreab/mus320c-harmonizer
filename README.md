Music 320C Project

Physical Model Based Harmonizer

- Physical model of vocal tract based off PinkTrombone: https://github.com/evykassirer/pink-trombone
- Pitch detection (adamski): https://github.com/adamski/pitch_detector
- Level detector (chowdhury-dsp): https://github.com/Chowdhury-DSP/chowdsp_utils/tree/master/DSP

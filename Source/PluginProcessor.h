/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "pitch_detector.h"
#include "GLOBAL.h"
#include "AudioSystem.h"
#include <vector>
#include "chowdsp_LevelDetector.h"'
#include "FreqToPitch.h"


//static const int MAX_BUFFER_SIZE = 512;
//static const size_t SAMPLE_RATE = 44100;

//==============================================================================
/**
*/
class HarmonizerPluginAudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    HarmonizerPluginAudioProcessor();
    ~HarmonizerPluginAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    //==============================================================================
    // pitch detection
    adamski::PitchMPM pitchMPM;
    float newPitch;
    
    //==============================================================================
    double inputGain; // input volume control
    float noteOnVel; // midi volume control
    double harmonyGain; // harmony volume control 
    
    //==============================================================================
    // vocal tract model
    AudioSystem audioSystem1;
    AudioSystem audioSystem2;
    AudioSystem audioSystem3;
    
    //==============================================================================
    std::map<int,float> chord; // for midi chords
    float chordNotes[4];
    float midiNoteTable[128];
    chowdsp::LevelDetector<float> levelDetector[2];
    
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (HarmonizerPluginAudioProcessor)
};

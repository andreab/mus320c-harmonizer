/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <numeric>
using namespace std;

#define MODE 2

//==============================================================================
HarmonizerPluginAudioProcessor::HarmonizerPluginAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
    ,pitchMPM(SAMPLE_RATE, MAX_BUFFER_SIZE)
    ,audioSystem1()
    ,audioSystem2()
    ,audioSystem3()
#endif
{
    // create midiNoteTable
    for (int midiNote = 0; midiNote < 128; midiNote++)
    {
        jassert(midiNote >= 0 && midiNote < 128);
        midiNoteTable[midiNote] = 440.0 * pow(2.0, (midiNote - 69.0)/12.0);
    }
}

HarmonizerPluginAudioProcessor::~HarmonizerPluginAudioProcessor()
{
}

//==============================================================================
const juce::String HarmonizerPluginAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool HarmonizerPluginAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool HarmonizerPluginAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool HarmonizerPluginAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double HarmonizerPluginAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int HarmonizerPluginAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int HarmonizerPluginAudioProcessor::getCurrentProgram()
{
    return 0;
}

void HarmonizerPluginAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String HarmonizerPluginAudioProcessor::getProgramName (int index)
{
    return {};
}

void HarmonizerPluginAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void HarmonizerPluginAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    
    
    juce::dsp::ProcessSpec spec;
    spec.numChannels = 1; spec.maximumBlockSize = samplesPerBlock; spec.sampleRate = sampleRate;
    levelDetector[0].prepare(spec);
    levelDetector[1].prepare(spec);
    levelDetector[0].setParameters(0.5, 10.0);
    levelDetector[1].setParameters(0.5, 10.0);
}

void HarmonizerPluginAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool HarmonizerPluginAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void HarmonizerPluginAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    // channel setup
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    // setup for pitch tracker
    int buffSize = buffer.getNumSamples();
    jassert(buffSize <= MAX_BUFFER_SIZE);
    pitchMPM.setBufferSize(buffSize); // set buffer size of pitchMPM
    //pitchMPM.setSampleRate(SAMPLE_RATE);
    
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    
  
    // process voice from audioSystem
    audioSystem1.doScriptProcessor(audioSystem1.noiseBufferIdx, buffer.getNumSamples());
    audioSystem2.doScriptProcessor(audioSystem2.noiseBufferIdx, buffer.getNumSamples());
    audioSystem3.doScriptProcessor(audioSystem3.noiseBufferIdx, buffer.getNumSamples());
    
    //std::vector<float> chord;
    
    // handle midi messages
    float currFreq;
    for (const auto metadata : midiMessages)
    {
        auto message = metadata.getMessage();
        
        // store midi notes in chord
        if (message.isNoteOn())
        {
            int note = message.getNoteNumber();
            float freq = message.getMidiNoteInHertz(note);
            chord.insert(std::pair<int, float>(note, freq));
        }
        else if (message.isNoteOff())
        {
            int note = message.getNoteNumber();
            float freq = message.getMidiNoteInHertz(note);
            chord.erase(note);
        }
    }
    
    // iterate through notes in chord and save in array
    std::map<int, float>::iterator itr;
    int i = 0;
    for (itr = chord.begin(); itr != chord.end(); ++itr) {
        chordNotes[i] = itr -> first;
        i++;
    }
    // if less than 4 notes are played, store remaining notes as 0
    if (i <= 3)
    {
        for (int j = i; j < 4 ; j++)
        {
            chordNotes[i] = 0;
        }
    }
    
    // sort notes in chord
    float chordNotesSorted[4];
    copy(begin(chordNotes), end(chordNotes), begin(chordNotesSorted));
    std::sort(chordNotesSorted,chordNotesSorted+4);
    
    // get min and max of chordFreqs
    float maxNote = chordNotesSorted[3];
    float minNote = chordNotesSorted[0];
    int idx = 1;
    while (minNote == 0)
    {
        minNote = chordNotesSorted[idx];
        idx++;
    }

    
// previous mode. harmonies controlled directly with midi input
#if MODE == 1
    int audioSystemGates[3];
    for (int i = 0; i < 3; i++)
    {
        if (chordFreqs[i] == 0)
            audioSystemGates[i] = 0;
        else
        {
            audioSystemGates[i] = 1;
            if (i == 0)
                audioSystem1.glottis.UIFrequency_ = chordFreqs[i];
            else if (i == 1)
                audioSystem2.glottis.UIFrequency_ = chordFreqs[i];
            else if (i == 2)
                audioSystem3.glottis.UIFrequency_ = chordFreqs[i];
        }
    }
#endif

// harmony logic based on midi input
#if MODE == 2
    int audioSystemGates[3] = {1,1,0};

#endif
    
    // loop through channels
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer (channel);
        auto* bufferPtr = buffer.getWritePointer (channel);
        auto* voiceOneBuffPtr = audioSystem1.outputBuffer.getWritePointer (0);
        auto* voiceTwoBuffPtr = audioSystem2.outputBuffer.getWritePointer (0);
        auto* voiceThreeBuffPtr = audioSystem3.outputBuffer.getWritePointer (0);
        
        // detect pitch from input buffer
        newPitch = pitchMPM.getPitch(buffer.getReadPointer(channel));
        
        if (newPitch != -1)
        {
            
        #if MODE == 2
            // calculate mini note from pitch
            int midiNote = 0.5 + log(newPitch/440.0)/log(2) * 12 + 69;
            
            // safety check
            if (midiNote >= 1 && midiNote <= 84)
            {
                //float tunedPitch = midiNoteTable[midiNote];
                
                // calcualte third and fifth notes of input note
                int noteInThird = midiNote+4; // maj third
                int noteInFifth = midiNote-5; // fifth (4th below)
                
                // shift note up or down to have it in the range of my 4 chord table
                int octShiftsThird = 0;
                int octShiftsFifth = 0;
                
                // third
                while (noteInThird < minNote)
                {
                    noteInThird += 12; // shift up an octave
                    octShiftsThird++; // keep track of octaves shifted
                }
                
                while (noteInThird > minNote+12)
                {
                    noteInThird -= 12; // shift down an octave
                    octShiftsThird--; // keep track of octaves shifted
                }

                // fifth
                while (noteInFifth < minNote)
                {
                    noteInFifth += 12; // shift up an octave
                    octShiftsFifth++; // keep track of octaves shifted
                }

                while (noteInFifth > minNote+12)
                {
                    noteInFifth -= 12; // shift down an octave
                    octShiftsFifth--; // keep track of octaves shifted
                }
                
                // check closest note to noteInThird that is in the 4 chord table
                int prevDistThird = abs(chordNotesSorted[0] - noteInThird);
                int noteHarm1 = chordNotesSorted[0];
                for (int i = 1; i < 4; i++)
                {
                    int currDistThird = abs(chordNotesSorted[i] - noteInThird);
                    if (currDistThird < prevDistThird)
                    {
                      noteHarm1 = chordNotesSorted[i];
                      prevDistThird = currDistThird;
                    }
                }
                // check closest note to noteInFifth that is in the 4 chord table
                int prevDistFifth = abs(chordNotesSorted[0] - noteInFifth);
                int noteHarm2 = chordNotesSorted[0];
                for (int i = 1; i < 4; i++)
                {
                    int currDistFifth = abs(chordNotesSorted[i] - noteInFifth);
                    if (currDistFifth < prevDistFifth)
                    {
                        noteHarm2 = chordNotesSorted[i];
                        prevDistFifth = currDistFifth;
                    }
                }

                // shift harmonies back up or down to the original octave
                noteHarm1 -= octShiftsThird*12;
                noteHarm2 -= octShiftsFifth*12;
                // convert to frequency and set freq of audioSystem1
                if (noteHarm1 != 0)
                {
                    float freqHarm1 = midiNoteTable[noteHarm1];
                    audioSystem1.glottis.UIFrequency_ = freqHarm1;
                }
                // convert to frequency and set freq of audioSystem2
                if (noteHarm2 != 0)
                {
                    float freqHarm2 = midiNoteTable[noteHarm2];
                    audioSystem2.glottis.UIFrequency_ = freqHarm2;
                }
            }
            
        #endif
        }

        // process buffer data (mic input and harmonies)
        for (int i = 0; i < buffer.getNumSamples(); i++)
        {
            // detect level of input signal
            float level = 3.0f*levelDetector[channel].processSample(abs(bufferPtr[i]));
        
            // compute output -> (mic input + harmonies)
            channelData[i] = level*(bufferPtr[i]*inputGain + ( voiceOneBuffPtr[i]*(harmonyGain/3)*audioSystemGates[0] + voiceTwoBuffPtr[i]*(harmonyGain/3)*audioSystemGates[1] + voiceThreeBuffPtr[i]*(harmonyGain/3)*audioSystemGates[2]));
        }
        
    }

}

//==============================================================================
bool HarmonizerPluginAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* HarmonizerPluginAudioProcessor::createEditor()
{
    return new HarmonizerPluginAudioProcessorEditor (*this);
}

//==============================================================================
void HarmonizerPluginAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void HarmonizerPluginAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}


//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new HarmonizerPluginAudioProcessor();
}

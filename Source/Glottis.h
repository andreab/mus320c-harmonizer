/*
  ==============================================================================

    Glottis.h
    Created: 20 May 2021 6:39:11pm
    Author:  Andy Baldioceda

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "GLOBAL.h"
#include "SimplexNoise.h"

#ifndef Glottis_h
#define Glottis_h

class Glottis{
    
public:
    float frequency_;
    float Rd_;
    float waveformLength_;
    
    float alpha_;
    float E0_;
    float epsilon_;
    float shift_;
    float Delta_;
    float Te_;
    float omega_;
    
    float timeInWaveform_ = 0;
    float oldFrequency_ = 140;
    float newFrequency_ = 140;
    float UIFrequency_ = 140;
    float smoothFrequency_ = 140;
    float oldTenseness_ = 0.6;
    float newTenseness_ = 0.6;
    float UITenseness_ = 0.6;
    float totalTime_ = 0;
    float vibratoAmount_ = 0.005;
    float vibratoFrequency_ = 6;
    float intensity_ = 0;
    float loudness_ = 1;
    bool isTouched_ = false;
// ctx : backCtx,
//    float touch_ = 0;
//    x : 240,
//    y : 530,

    int semitones_ = 20;
  //  marks : [0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    float baseNote_ = 87.3071; //F
    
    void init();
    void setupWaveform(float lambda);
    float normalizedLFWaveform(float t);
    float runStep(float lambda, float noiseSource);
    float getNoiseModulator();
    void finishBlock();
    
    SimplexNoise sNoise;

};



#endif /* Glottis_h */

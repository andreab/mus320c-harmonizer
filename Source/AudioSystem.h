/*
  ==============================================================================

    AudioSystem.h
    Created: 20 May 2021 6:38:17pm
    Author:  Andy Baldioceda

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "GLOBAL.h"
#include "Glottis.h"
#include "Tract.h"

#ifndef AudioSystem_h
#define AudioSystem_h


class AudioSystem{
    
public:
    int blockLength = 512;
    int blockTime = 1;
    bool started = false;
    bool soundOn = false;
    
    juce::AudioBuffer<float> filteredNoise1;
    juce::AudioBuffer<float> filteredNoise2;
    
    juce::AudioBuffer<float> outputBuffer;
    int noiseBufferIdx;
    
    Glottis glottis;
    Tract tract;
    
    AudioSystem();
    //void setup();
    void startSound();
    juce::AudioBuffer<float> createWhiteNoiseBuffer(int frameCount);
    
    void doScriptProcessor(int startPos, int numSamples);
    
    void mute();
    void unmute();
    
private:
    juce::Random random;
    
};

#endif /* AudioSystem_h */

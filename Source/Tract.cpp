/*
  ==============================================================================

    Tract.cpp
    Created: 20 May 2021 6:39:31pm
    Author:  Andy Baldioceda

  ==============================================================================
*/
#include <stdio.h>
#include <time.h>
#include <iostream>
#include "Tract.h"
#include "Math.h"

using namespace std;

//==============================================================================
void Tract::init()
{
    
    bladeStart = floor(bladeStart*n/44);
    tipStart = floor(tipStart*n/44);
    lipStart = floor(lipStart*n/44);

    for (int i = 0; i < n; i++)
    {
        float diameterTemp = 0;
        if (i < 7*n/44-0.5) diameterTemp = 0.6;
        else if (i < 12*n/44) diameterTemp = 1.1;
        else diameterTemp = 1.5;
        diameter[i] = restDiameter[i] = targetDiameter[i] = newDiameter[i] = diameterTemp;
    }
    
    for (int i = 0; i < noseLength; i++)
    {
        float diameterTemp;
        float d = 2*(i/noseLength);
        if (d < 1) diameterTemp = 0.4 + 1.6*d;
        else diameterTemp = 0.5 + 1.5*(2-d);
        diameterTemp = fmin(diameterTemp, 1.9);
        noseDiameter[i] = diameterTemp;
    }
    
    newReflectionLeft = newReflectionRight = newReflectionNose = 0;
    calculateReflections();
    calculateNoseReflections();
    noseDiameter[0] = velumTarget;
}

//==============================================================================
void Tract::reshapeTract(float deltaTime)
{
    float amount = deltaTime * movementSpeed; ;
    int newLastObstruction = -1;
    for (int i = 0; i < n; i++)
    {
        float diameterTemp = diameter[i];
        float targetDiameterTemp = targetDiameter[i];
        if (diameterTemp <= 0) newLastObstruction = i;
        float slowReturn;
        if (i < noseStart) slowReturn = 0.6;
        else if (i >= tipStart) slowReturn = 1.0;
        else slowReturn = 0.6+0.4*(i-noseStart)/(tipStart-noseStart);
        diameter[i] = moveTowards(diameterTemp, targetDiameterTemp, slowReturn*amount, 2*amount);
    }
    if (lastObstruction>-1 && newLastObstruction == -1 && noseA[0]<0.05)
    {
        addTransient(lastObstruction);
    }
    lastObstruction = newLastObstruction;

    amount = deltaTime * movementSpeed;
    noseDiameter[0] = moveTowards(noseDiameter[0], velumTarget, amount*0.25, amount*0.1);
    noseA[0] = noseDiameter[0]*noseDiameter[0];
    
}

//==============================================================================
void Tract::calculateReflections()
{
    for (int i = 0; i < n; i++)
    {
        A[i] = diameter[i] * diameter[i]; //ignoring PI etc.
    }
    for (int i = 1; i < n; i++)
    {
        reflection[i] = newReflection[i];
        if (A[i] == 0) newReflection[i] = 0.999; //to prevent some bad behaviour if 0
        else newReflection[i] = (A[i-1] - A[i]) / (A[i-1] + A[i]); // reflection cofficient
    }

    //now at junction with nose
    
    reflectionLeft = newReflectionLeft;
    reflectionRight = newReflectionRight;
    reflectionNose = newReflectionNose;
    float sum = A[noseStart] + A[noseStart + 1] + noseA[0];
    newReflectionLeft = (2*A[noseStart] - sum) / sum;
    newReflectionRight = (2*A[noseStart+1] - sum) / sum;
    newReflectionNose = (2*noseA[0] - sum) / sum;
}

//==============================================================================
void Tract:: calculateNoseReflections()
{
    for (int i = 0; i < noseLength; i++)
    {
        noseA[i] = noseDiameter[i] * noseDiameter[i];
    }
    for (int i = 1; i < noseLength; i++)
    {
        noseReflection[i] = (noseA[i-1] - noseA[i]) / (noseA[i-1] + noseA[i]); // reflection coefficient
    }
}

//==============================================================================
void Tract::runStep(float glottalOutput, float turbulenceNoise, float lambda)
{
    srand( (unsigned)time( NULL ) );
    float randNum = (rand()/RAND_MAX); // random number between 0 and 1
    
    bool updateAmplitudes = (randNum < 0.1); //(Math.random()<0.1);

    //mouth
    processTransients();
    
    // TODO: might add later
    // addTurbulenceNoise(turbulenceNoise);

    junctionOutputR[0] = L[0] * glottalReflection + glottalOutput;
    junctionOutputL[n] = R[n-1] * lipReflection;

    for (int i = 1; i < n; i++)
    {
        float r = reflection[i] * (1-lambda) + newReflection[i]*lambda;
        float w = r * (R[i-1] + L[i]);
        junctionOutputR[i] = R[i-1] - w;
        junctionOutputL[i] = L[i] + w;
    }
    
    //now at junction with nose
    int i = noseStart;
    float r = newReflectionLeft * (1-lambda) + reflectionLeft*lambda;
    junctionOutputL[i] = r*R[i-1]+(1+r)*(noseL[0]+L[i]);
    r = newReflectionRight * (1-lambda) + reflectionRight*lambda;
    junctionOutputR[i] = r*L[i]+(1+r)*(R[i-1]+noseL[0]);
    r = newReflectionNose * (1-lambda) + reflectionNose*lambda;
    noseJunctionOutputR[0] = r*noseL[0]+(1+r)*(L[i]+R[i-1]);
    
    for (int i = 0; i < n; i++)
    {
        R[i] = junctionOutputR[i]*0.999;
        L[i] = junctionOutputL[i+1]*0.999;

        if (updateAmplitudes)
        {
            float amplitude = abs(R[i]+L[i]);
            if (amplitude > maxAmplitude[i]) maxAmplitude[i] = amplitude;
            else maxAmplitude[i] *= 0.999;
        }
    }
    
    lipOutput = R[n-1];

    //nose
    noseJunctionOutputL[noseLength] = noseR[noseLength-1] * lipReflection;

    for (int i = 1; i < noseLength; i++)
    {
        float w = noseReflection[i] * (noseR[i-1] + noseL[i]);
        noseJunctionOutputR[i] = noseR[i-1] - w;
        noseJunctionOutputL[i] = noseL[i] + w;
    }

    for (int i = 0; i < noseLength; i++)
    {
        noseR[i] = noseJunctionOutputR[i] * fade;
        noseL[i] = noseJunctionOutputL[i+1] * fade;

        if (updateAmplitudes)
        {
            float amplitude = abs(noseR[i]+noseL[i]);
            if (amplitude > noseMaxAmplitude[i]) noseMaxAmplitude[i] = amplitude;
            else noseMaxAmplitude[i] *= 0.999;
        }
    }

    noseOutput = noseR[noseLength-1];
    
}

//==============================================================================
void Tract::finishBlock(float blockTime)
{
    reshapeTract(blockTime);
    calculateReflections();
}

//==============================================================================
void Tract::addTransient(int position)
{
    trans transient;
    transient.position = position;
    transient.timeAlive = 0;
    transient.lifeTime = 0.2;
    transient.strength = 0.3;
    transient.exponent = 200;
    
    transients.push_back(transient);
}

//==============================================================================
void Tract::processTransients()
{
    for (int i = 0; i < transients.size(); i++)
    {
        trans transient = transients[i];
        float amplitude = transient.strength * pow(2, -transient.exponent * transient.timeAlive);
        R[transient.position] += amplitude/2;
        L[transient.position] += amplitude/2;
        transient.timeAlive += 1.0/(SAMPLE_RATE*2);
    }
    for (int i = transients.size()-1; i >= 0; i--)
    {
        trans transient = transients[i];
        if (transient.timeAlive > transient.lifeTime)
        {
            // TODO: verify this is equivalent to transients.splice(i,1) in javascript
            transients.erase(transients.begin() + i);// transients.splice(i,1); in javascript: deletes 1 element at index i
        }
    }

}

//==============================================================================
void Tract::addTurbulenceNoise(float turbulenceNoise)
{
    //
}

//==============================================================================
void Tract::addTurbulenceNoiseAtIndex(float turbulenceNoise, int index, float diameter)
{
    //
}

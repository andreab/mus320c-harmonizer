//
//  GLOBAL.h
//  HarmonizerPlugin - All
//
//  Created by Andy Baldioceda on 5/20/21.
//

#ifndef GLOBAL_h
#define GLOBAL_h

static const int MAX_BUFFER_SIZE = 2048;
static const size_t SAMPLE_RATE = 44100;
static bool AUTO_WOBBLE = false;
static bool ALWAYS_VOICE = true;

#endif /* GLOBAL_h */

/*
  ==============================================================================

    Math.h
    Created: 26 May 2021 6:27:20pm
    Author:  Andy Baldioceda

  ==============================================================================
*/
#pragma once
#include <math.h>

static float clamp(float number, float min, float max)
{
    if (number<min) return min;
    else if (number>max) return max;
    else return number;
}

static float moveTowards(float current, float target, float amount)
{
    if (current<target) return fmin(current+amount, target);
    else return fmax(current-amount, target);
}

static float moveTowards(float current, float target, float amountUp, float amountDown)
{
    if (current<target) return fmin(current+amountUp, target);
    else return fmax(current-amountDown, target);
}

static float gaussian()
{
    srand( (unsigned)time( NULL ) );
    float randNum = (rand()/RAND_MAX); // random number between 0 and 1
    
    float s = 0;
    for (int c = 0; c < 16; c++) s += randNum;//javascript: Math.random();
    return (s-8)/4;
}

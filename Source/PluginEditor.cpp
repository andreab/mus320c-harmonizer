/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
HarmonizerPluginAudioProcessorEditor::HarmonizerPluginAudioProcessorEditor (HarmonizerPluginAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (500, 200);
    
    inputGainSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    inputGainSlider.setTextBoxStyle(juce::Slider::TextBoxRight, true, 80, 25);
    inputGainSlider.setRange(0.0,0.9);
    inputGainSlider.setValue(0.5);
    inputGainSlider.addListener(this);
    
    harmonyGainSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    harmonyGainSlider.setTextBoxStyle(juce::Slider::TextBoxRight, true, 80, 25);
    harmonyGainSlider.setRange(0.0,0.9);
    harmonyGainSlider.setValue(0.5);
    harmonyGainSlider.addListener(this);
    
    addAndMakeVisible(inputGainSlider);
    addAndMakeVisible(harmonyGainSlider);
    addAndMakeVisible (inputGainLabel);
    addAndMakeVisible (harmonyGainLabel);
    
    inputGainLabel.setText ("Mic Input Gain", juce::dontSendNotification);
    inputGainLabel.attachToComponent (&inputGainSlider, true);
    harmonyGainLabel.setText ("Harmony Gain", juce::dontSendNotification);
    harmonyGainLabel.attachToComponent (&harmonyGainSlider, true);
}

HarmonizerPluginAudioProcessorEditor::~HarmonizerPluginAudioProcessorEditor()
{
}

//==============================================================================
void HarmonizerPluginAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
    g.setFont (18.0f);
    g.drawFittedText ("Physical Model Based Harmonizer", 0, 0, getWidth(), 30, juce::Justification::centred, 1);
}

void HarmonizerPluginAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    inputGainSlider.setBounds(80,70,getWidth() - 80, 20);
    harmonyGainSlider.setBounds(80,30,getWidth() - 80, 20);
}

void HarmonizerPluginAudioProcessorEditor::sliderValueChanged(juce::Slider* slider)
{
    if (slider == &inputGainSlider)
    {
        audioProcessor.inputGain = inputGainSlider.getValue();
    }
    else if (slider == &harmonyGainSlider)
    {
        audioProcessor.harmonyGain = harmonyGainSlider.getValue();
    }

}

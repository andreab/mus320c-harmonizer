/*
  ==============================================================================

    AudioSystem.cpp
    Created: 20 May 2021 6:38:52pm
    Author:  Andy Baldioceda

  ==============================================================================
*/
#include <stdio.h>
#include "AudioSystem.h"

//==============================================================================
AudioSystem::AudioSystem(void)
{
    blockTime = blockLength/SAMPLE_RATE;

    // create white noise
    filteredNoise1.makeCopyOf(createWhiteNoiseBuffer(2*SAMPLE_RATE)); // createWhiteNoiseBuffer(2*SAMPLE_RATE)
    filteredNoise2.makeCopyOf(createWhiteNoiseBuffer(2*SAMPLE_RATE));
    
    noiseBufferIdx = 0;

    // create fitlers
    juce::IIRFilter aspirateFilt;
    aspirateFilt.setCoefficients(juce::IIRCoefficients::makeBandPass(SAMPLE_RATE, 500, 0.5)); // (sample rate, frequency, Q)

    juce::IIRFilter fricativeFilt;
    aspirateFilt.setCoefficients(juce::IIRCoefficients::makeBandPass(SAMPLE_RATE, 1000, 0.5)); // (sample rate, frequency, Q)

    // process white noise with filters
    aspirateFilt.processSamples(filteredNoise1.getWritePointer(0), filteredNoise1.getNumSamples());
    fricativeFilt.processSamples(filteredNoise2.getWritePointer(0), filteredNoise2.getNumSamples());

    // set size of output buffer
    outputBuffer.setSize(1, filteredNoise1.getNumSamples());
    
    // initialize Glottis
    glottis.init();
    tract.init();
}


//==============================================================================
//void AudioSystem::startSound()
//{
//
//}
//

//==============================================================================
juce::AudioBuffer<float> AudioSystem::createWhiteNoiseBuffer (int frameCount)
{
    juce::AudioBuffer<float> whiteNoise(1, frameCount);

    for (int i = 0; i < frameCount; i ++)
    {
        whiteNoise.setSample(0, i, random.nextFloat()*0.25f-0.125f);
    }

    return whiteNoise;
}


//==============================================================================
void AudioSystem::doScriptProcessor(int startPos, int numSamples)
{

    int i = 0; // index for outputBuffer
    int idx; // index for noise buffer
    int N = numSamples;
    for (int j = startPos; j < startPos+numSamples; j++)
    {
        float lambda1 = i*1.0/N;
        float lambda2 = (i*1.0+0.5)/N;
         
        // check index of filteredNoise1
        if (j >= filteredNoise1.getNumSamples()) idx = j % filteredNoise1.getNumSamples();
        else idx = j;
        noiseBufferIdx = idx;
        
        // calculate glotal output
        float glottalOutput = glottis.runStep(lambda1, filteredNoise1.getSample(0, idx));
        
        // calculate vocal output from tract
        float vocalOutput = 0;
        //Tract runs at twice the sample rate
        tract.runStep(glottalOutput, filteredNoise2.getSample(0, idx), lambda1);
        vocalOutput += tract.lipOutput + tract.noseOutput;
        tract.runStep(glottalOutput, filteredNoise2.getSample(0, idx), lambda2);
        vocalOutput += tract.lipOutput + tract.noseOutput;

        // comput output sample
        float outSample = vocalOutput * 0.125;
        outputBuffer.setSample(0, i, outSample);
        
        i++;
    }

    glottis.finishBlock();
    tract.finishBlock(blockTime);
}


//==============================================================================
void AudioSystem::mute()
{
    // mute noise
}


//==============================================================================
void AudioSystem::unmute()
{
  // unmute noise
}


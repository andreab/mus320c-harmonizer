/*
  ==============================================================================

    Tract.h
    Created: 20 May 2021 6:39:37pm
    Author:  Andy Baldioceda

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "GLOBAL.h"
#include <math.h>
#include <vector>

#ifndef Tract_h
#define Tract_h

class Tract
{
    
public:
    static const int n = 44;
    int bladeStart = 10; // not in use
    int tipStart = 32;//32;
    int lipStart = 39;//39;
    
    // TODO: change to std::arrays std::array<float>?
    
    float R[n] = {0}; //component going right
    float L[n] = {0}; //component going left
    float reflection[n+1] = {0};
    float newReflection[n+1] = {0};
    float junctionOutputR[n+1] = {0};
    float junctionOutputL[n+1] = {0};
    float A[n] = {0};
    float maxAmplitude[n] = {0};
    
    float diameter[n] = {0};
    float restDiameter[n] = {0};
    float targetDiameter[n] = {0};
    float newDiameter[n] = {0};
    
    static const int noseLength = 28; // floor(28*n/44) -- but n=44 and doesn't change
    int noseStart = n-noseLength+1;
    float noseR[noseLength] = {0};
    float noseL[noseLength] = {0};
    float noseJunctionOutputR[noseLength+1] = {0};
    float noseJunctionOutputL[noseLength+1] = {0};
    float noseReflection[noseLength+1] = {0};
    float noseDiameter[noseLength] = {0};
    float noseA[noseLength] = {0};
    float noseMaxAmplitude[noseLength] = {0};
    
    float reflectionNose;
    float reflectionRight;
    float reflectionLeft;
    
    float newReflectionNose;
    float newReflectionRight;
    float newReflectionLeft;
    
    float glottalReflection = 0.75;
    float lipReflection = -0.85;
    int   lastObstruction = -1;
    float fade = 1.0; //0.9999,
    float movementSpeed = 15; //cm per second
    
    float lipOutput = 0;
    float noseOutput = 0;
    float velumTarget = 0.01;
    
    struct trans
    {
        int position;
        float timeAlive;
        float lifeTime;
        float strength;
        float exponent;
    };
    
    std::vector<trans> transients;
    
    void init();
    void reshapeTract(float deltaTime);
    void calculateReflections();
    void calculateNoseReflections();
    void runStep(float glottalOutput, float turbulenceNoise, float lambda);
    void finishBlock(float blockTime);
    void addTransient(int position);
    void processTransients();
    void addTurbulenceNoise(float turbulenceNoise);
    void addTurbulenceNoiseAtIndex(float turbulenceNoise, int index, float diameter);
    
    
  
};


#endif /* Tract_h */

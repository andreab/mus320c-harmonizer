/*
  ==============================================================================

    Glottis.cpp
    Created: 20 May 2021 6:39:04pm
    Author:  Andy Baldioceda

  ==============================================================================
*/
#include <stdio.h>
#include <math.h>
#include "Glottis.h"
#include "Math.h"
//==============================================================================
void Glottis::init()
{
    setupWaveform(0);
}

//==============================================================================
void Glottis::setupWaveform(float lambda)
{
    frequency_ = oldFrequency_*(1-lambda) + newFrequency_*lambda;
    float tenseness = oldTenseness_*(1-lambda) + newTenseness_*lambda;
    Rd_ = 3*(1-tenseness);
    waveformLength_ = 1.0/frequency_;
    
    float Rd = Rd_;
    if (Rd<0.5) Rd = 0.5;
    if (Rd>2.7) Rd = 2.7;
    
    // normalized to time = 1, Ee = 1
    float Ra = -0.01 + 0.048*Rd;
    float Rk = 0.224 + 0.118*Rd;
    float Rg = (Rk/4)*(0.5+1.2*Rk)/(0.11*Rd-Ra*(0.5+1.2*Rk));
    
    float Ta = Ra;
    float Tp = 1 / (2*Rg);
    float Te = Tp + Tp*Rk; //

    float epsilon = 1/Ta;
    float shift = exp(-epsilon * (1-Te));
    float Delta = 1 - shift; //divide by this to scale RHS
    
    float RHSIntegral = (1/epsilon)*(shift - 1) + (1-Te)*shift;
    RHSIntegral = RHSIntegral/Delta;

    float totalLowerIntegral = - (Te-Tp)/2 + RHSIntegral;
    float totalUpperIntegral = -totalLowerIntegral;

    float omega = M_PI/Tp;
    float s = sin(omega*Te);
    
    float y = -M_PI*s*totalUpperIntegral / (Tp*2);
    float z = log(y);
    float alpha = z/(Tp/2 - Te);
    float E0 = -1 / (s*exp(alpha*Te));
    
    alpha_ = alpha;
    E0_ = E0;
    epsilon_ = epsilon;
    shift_ = shift;
    Delta_ = Delta;
    Te_ = Te;
    omega_ = omega;
    
}

//==============================================================================
float Glottis::normalizedLFWaveform(float t)
{
    float output;
    if (t>Te_) output = (-exp(-epsilon_ * (t-Te_)) + shift_)/Delta_;
    else output = E0_ * exp(alpha_ * t) * sin(omega_ * t);
    
    return output * intensity_ * loudness_;
}

//==============================================================================
float Glottis::runStep(float lambda, float noiseSource)
{
    
    float timeStep = 1.0/SAMPLE_RATE;
    timeInWaveform_ += timeStep;
    totalTime_ += timeStep;
    if (timeInWaveform_ > waveformLength_)
    {
        timeInWaveform_ -= waveformLength_;
        setupWaveform(lambda);
    }
    
    float out = normalizedLFWaveform(timeInWaveform_/waveformLength_);
    float aspiration = intensity_*(1-sqrt(UITenseness_))*getNoiseModulator()*noiseSource;
    aspiration *= 0.2 + 0.02*sNoise.noise(totalTime_ * 1.99);
    
    out += aspiration;
    return out;
}

//==============================================================================
float Glottis::getNoiseModulator()
{
    float voiced = 0.1+0.2*fmax(0,sin(M_PI*2*timeInWaveform_/waveformLength_));
        //return 0.3;
    
    return UITenseness_ * intensity_ * voiced + (1-UITenseness_* intensity_) * 0.3;
    
}

//==============================================================================
void Glottis::finishBlock()
{
    float vibrato = 0;
    vibrato += vibratoAmount_ * sin(2*M_PI * totalTime_ * vibratoFrequency_);
    
    vibrato += 0.001 * sNoise.noise(totalTime_ * 2.07);
    vibrato += 0.002 * sNoise.noise(totalTime_ * 1.15);
//    vibrato += 0.02 * sNoise.noise(totalTime_ * 4.07);
//    vibrato += 0.04 * sNoise.noise(totalTime_ * 2.15);
    
    if (AUTO_WOBBLE)
    {
        vibrato += 0.2 * sNoise.noise(totalTime_ * 0.98);
        vibrato += 0.4 * sNoise.noise(totalTime_ * 0.5);
}
    if (UIFrequency_ > smoothFrequency_)
        smoothFrequency_ = fmin(smoothFrequency_ * 1.1, UIFrequency_);
    if (UIFrequency_ < smoothFrequency_)
        smoothFrequency_ = fmax(smoothFrequency_ / 1.1, UIFrequency_);
    oldFrequency_ = newFrequency_;
    newFrequency_ = smoothFrequency_ * (1+vibrato);
    oldTenseness_ = newTenseness_;
    newTenseness_ = UITenseness_ + 0.1*sNoise.noise(totalTime_*0.46)+0.05*sNoise.noise(totalTime_*0.36);
    
    if (!isTouched_ && ALWAYS_VOICE) newTenseness_ += (3-UITenseness_)*(1-intensity_);
    if (isTouched_ || ALWAYS_VOICE) intensity_ += 0.13;
    else intensity_ -= 0.05;
    intensity_ = clamp(intensity_, 0, 1);
}
